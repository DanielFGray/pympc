#!/usr/bin/env python3

import os
import gi
import socket
from mpd import MPDClient
from gi.repository import Gtk

gi.require_version('Gtk', '3.0')

client = MPDClient()
client.connect("localhost", 6600)

class Controls(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title='PyMPC')

        self.box = Gtk.Box(spacing=6)
        self.add(self.box)

        self.prev_button = Gtk.Button(label="Prev")
        self.prev_button.connect("clicked", self.prev_button_clicked)
        self.box.pack_start(self.prev_button, True, True, 0)

        self.pause_button = Gtk.Button(label="Pause")
        self.pause_button.connect("clicked", self.pause_button_clicked)
        self.box.pack_start(self.pause_button, True, True, 0)

        self.next_button = Gtk.Button(label="Next")
        self.next_button.connect("clicked", self.next_button_clicked)
        self.box.pack_start(self.next_button, True, True, 0)

    def next_button_clicked(self, widget):
        client.next()

    def pause_button_clicked(self, widget):
        client.pause()

    def prev_button_clicked(self, widget):
        client.previous()


window = Controls()
window.connect('delete-event', Gtk.main_quit)
window.show_all()
Gtk.main()
